
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { AddPostComponent } from './components/add-post/add-post.component';
import { HomeComponent } from './components/home/home.component';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddIncomeComponent } from './components/add-income/add-income.component';
import { EditIncomeComponent } from './components/edit-income/edit-income.component';
import { AddExpenseComponent } from './components/add-expense/add-expense.component';
import { EditExpenseComponent } from './components/edit-expense/edit-expense.component';
import { IncomesService } from './services/incomes.service';
import { ExpensesService } from './services/expenses.service';
import { PostsService } from './services/posts.service';
import { ConfigService } from './services/config.service';
import { UtilsService } from './services/utils.service';
import { ListIncomesComponent } from './components/list-incomes/list-incomes.component';
import { ListExpensesComponent } from './components/list-expenses/list-expenses.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    AddPostComponent,
    HomeComponent,
    EditPostComponent,
    AddIncomeComponent,
    EditIncomeComponent,
    AddExpenseComponent,
    EditExpenseComponent,
    ListIncomesComponent,
    ListExpensesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'addincome',
        component: AddIncomeComponent
      },
      {
        path: 'addexpense',
        component: AddExpenseComponent
      },
      {
        path: 'editincome/:id',
        component: EditIncomeComponent
      },
      {
        path: 'editexpense/:id',
        component: EditExpenseComponent
      },
      {
        path: 'addpost',
        component: AddPostComponent
      },
      {
        path: 'editpost/:id',
        component: EditPostComponent
      },
      {
        path: '**',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-bottom-right',
    })
  ],
  providers: [
    PostsService,
    IncomesService,
    ExpensesService,
    ConfigService,
    UtilsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
