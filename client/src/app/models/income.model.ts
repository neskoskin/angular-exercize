export interface Income {
    _id: string;
    description: string;
    // type: string;
    amount: number;
}