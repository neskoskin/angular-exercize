export interface Expense {
    _id: string;
    description: string;
    // type: string;
    amount: number;
}