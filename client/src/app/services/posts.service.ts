import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostsService {
  private url = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) { }

  fetchPosts() {
    return this.http.get(this.url);
  }

  fetchPost(id) {
    return this.http.get(`${this.url}/${id}`);
  }

  addPost(post)  {
    return this.http.post(this.url, post);
  }

  updatePost(id, post) {
    return this.http.put(`${this.url}/${id}`, post);
  }

  deletePost(id) {
    return this.http.delete(`${this.url}/${id}`);
  }

}
