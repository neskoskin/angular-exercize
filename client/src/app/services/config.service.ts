import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {

	constructor() { }

	public baseUrl(): string {
		return environment.baseUrl;
	}

	public fullUrl(url): string {
		return this.baseUrl() + url;
	}

	public production(): boolean {
		return environment.production;
	}
}