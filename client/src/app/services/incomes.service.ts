import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Income } from '../models/income.model';
import { of } from 'rxjs/observable/of';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from '../services/config.service';
import { DataResponse } from '../interfaces/dataResponse.interface';
import { UtilsService } from './utils.service';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class IncomesService {
  incomeValueChange = new BehaviorSubject('default data');
  constructor(
    private http: HttpClient,
    private conf: ConfigService,
    private utils: UtilsService) {
  }

  getIncomeChange(data) {
    this.incomeValueChange.next(data);
  }
  addIncome(income: Income): Observable<Income> {
    return this.http.post(
      this.conf.fullUrl('/income'),
      income
    ).pipe(
      catchError(error => {
        return of(error);
      })
    )
  }
  findIncome(incomeId: string): Observable<Income> {
    return this.http.get(
      this.conf.fullUrl('/income/') + incomeId
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  updateIncome(income: Income): Observable<Income> {
    return this.http.post(
      this.conf.fullUrl('/income/') + income._id,
      income
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  deleteIncome(incomeId: string) {
    return this.http.delete(
      this.conf.fullUrl('/income/') + incomeId
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  listIncomes(filter?: any): Observable<DataResponse<Income[]>> {
    return this.http.get(
      this.conf.fullUrl('/incomes/') + this.utils.paramterize(filter, true)
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
}
