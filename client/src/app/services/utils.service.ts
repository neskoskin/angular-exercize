import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {

	constructor() { }

	public paramterize(obj: any, withQMark: boolean = false): string {
		if (this.nil(obj)) {
			return '';
		}
		const paramsArray = [];
		let finalStr = '';
		Object.keys(obj).forEach(key => {
			if (!this.nil(obj[key]) && obj[key] !== '') {
				paramsArray.push(`${key}=${encodeURIComponent(obj[key])}`);
			}
		});
		if (paramsArray.length) {
			finalStr = paramsArray.join('&');
		}
		if (withQMark) {
			finalStr = '?' + finalStr;
		}
		return finalStr;
	}

	public nil(value: any): boolean {
		return value === undefined || value === null;
	}

	public cleanParams(obj: any): any {
		Object.keys(obj).forEach(key => {
			if (!(!this.nil(obj[key]) && obj[key] !== '' && (typeof obj[key] === 'string' || typeof obj[key] === 'number'))) {
				delete obj[key];
			}
		});

		return obj;
	}

	public range(start: number, end: number): number[] {
		return Array.from({ length: (end - start) }, (v, k) => k + start);
	}
}