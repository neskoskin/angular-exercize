import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Expense } from '../models/expense.model';
import { of } from 'rxjs/observable/of';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from '../services/config.service';
import { DataResponse } from '../interfaces/dataResponse.interface';
import { UtilsService } from './utils.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ExpensesService {
  expenseValueChange = new BehaviorSubject('default data');
  constructor(
    private http: HttpClient,
    private conf: ConfigService,
    private utils: UtilsService) { }
  getExpenseChange(data) {
    this.expenseValueChange.next(data);
  }
  addExpense(expense: Expense): Observable<Expense> {
    return this.http.post(
      this.conf.fullUrl('/expense'),
      expense
    ).pipe(
      catchError(error => {
        return of(error);
      })
    )
  }
  findExpense(expenseId: string): Observable<Expense> {
    return this.http.get(
      this.conf.fullUrl('/expense/') + expenseId
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  updateExpense(expense: Expense): Observable<Expense> {
    console.log('in update expense', expense);
    return this.http.post(
      this.conf.fullUrl('/expense/') + expense._id,
      expense
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  deleteExpense(expenseId: string) {
    return this.http.delete(
      this.conf.fullUrl('/expense/') + expenseId
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
  listExpenses(filter?: any): Observable<DataResponse<Expense[]>> {
    return this.http.get(
      this.conf.fullUrl('/expenses/') + this.utils.paramterize(filter, true)
    )
      .pipe(
        catchError(error => {
          return of(error);
        })
      );
  }
}
