import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RouterModule, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ExpensesService } from '../../services/expenses.service';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css']
})
export class AddExpenseComponent implements OnInit {
  addExpenseForm: FormGroup;
  // showError = false;
  // showSuccess = false;

  constructor(private expensesService: ExpensesService, private router: Router, private toastr: ToastrService) { }

  showSuccess() {
    return this.toastr.success('Expense successfully added!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }

  addExpense() {

    this.expensesService.addExpense(this.addExpenseForm.value)
      .subscribe(response => {
        if (response) {
          this.showSuccess();
          this.expensesService.getExpenseChange(response);
          this.resetForm();
        } else {
          this.showError();
        }
      });
  }
  resetForm() {
    this.addExpenseForm = new FormGroup({
      description: new FormControl('', [Validators.required]),
      amount: new FormControl(0, [Validators.required, Validators.min(0)]),
    });
  }

  ngOnInit() {
    this.resetForm();
  }

}
