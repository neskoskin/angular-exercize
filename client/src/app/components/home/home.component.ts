import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  expenseSum: number;
  incomeSum: number;
  totalBalance: number;
  constructor() { }

  ngOnInit() {
     
  }
  incomeChangedHandler(incomeSumValue) {
    console.log('income changed', incomeSumValue);
    this.incomeSum = incomeSumValue;
  }
  expenseChangedHandler(expenseSumValue) {
    console.log('expense changed', expenseSumValue);
    this.expenseSum = expenseSumValue;
  }
}
