import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RouterModule, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IncomesService } from '../../services/incomes.service';

@Component({
  selector: 'app-add-income',
  templateUrl: './add-income.component.html',
  styleUrls: ['./add-income.component.css']
})
export class AddIncomeComponent implements OnInit {
  addIncomeForm: FormGroup;
  // showError = false;
  // showSuccess = false;

  constructor(private incomesService: IncomesService, private router: Router, private toastr: ToastrService) { }

  showSuccess() {
    return this.toastr.success('Income successfully added!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }

  addIncome() {

    this.incomesService.addIncome(this.addIncomeForm.value)
      .subscribe(response => {
        if (response) {
          this.showSuccess();
          this.incomesService.getIncomeChange(response);
          this.resetForm();
        } else {
          this.showError();
        }
      });
  }
  resetForm() {
    this.addIncomeForm = new FormGroup({
      description: new FormControl('', Validators.required),
      amount: new FormControl(0, [Validators.required, Validators.min(0)]),
    });
  }
  ngOnInit() {
    this.resetForm();
  }
}
