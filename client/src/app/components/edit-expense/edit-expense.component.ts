import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ExpensesService } from '../../services/expenses.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.css']
})
export class EditExpenseComponent implements OnInit {
  id: number;
  public editExpenseForm: FormGroup = null;

  constructor(private expensesService: ExpensesService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.setInitialFormFields();
    this.fetchExpense(this.id);
  }
  showSuccess() {
    return this.toastr.success('Expense successfully updated!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }
  fetchExpense(id) {
    this.expensesService.findExpense(id).subscribe(response => {
      const data = response;
      this.editExpenseForm.controls.description.setValue(data.description);

      this.editExpenseForm.controls.amount.setValue(data.amount);
    });
  }

  updateExpense() {
    this.editExpenseForm.value._id = this.id;
    this.expensesService.updateExpense(this.editExpenseForm.value).subscribe(response => {
      if (response._id != undefined) {
        this.showSuccess().onHidden.subscribe(res => {
          this.expensesService.getExpenseChange(response);
          this.router.navigate(['/home']);
        })
      } else {
        // show error
        this.showError();
      }
    });
  }

  setInitialFormFields() {
    this.editExpenseForm = new FormGroup({
      description: new FormControl('', Validators.required),
      amount: new FormControl(0, [Validators.required, Validators.min(0)])
    });
  }

}
