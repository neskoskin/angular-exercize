import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomesService } from '../../services/incomes.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-income',
  templateUrl: './edit-income.component.html',
  styleUrls: ['./edit-income.component.css']
})
export class EditIncomeComponent implements OnInit {
  id: number;
  data: any;
  public editIncomeForm: FormGroup = null;
  @Output() updatedIncome = new EventEmitter<Object>();
  constructor(private incomesService: IncomesService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService, ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.setInitialFormFields();
    this.fetchIncome(this.id);
  }
  showSuccess() {
    return this.toastr.success('Income successfully updated!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }

  fetchIncome(id) {
    this.incomesService.findIncome(id).subscribe(response => {
      const data = response;

      this.editIncomeForm.controls.description.setValue(data.description);

      this.editIncomeForm.controls.amount.setValue(data.amount);
    });
  }

  updateIncome() {
    this.editIncomeForm.value._id = this.id;
    this.incomesService.updateIncome(this.editIncomeForm.value).subscribe(response => {
      if (response._id != undefined) {
        this.showSuccess().onHidden.subscribe(res => {
          this.incomesService.getIncomeChange(response);
          this.router.navigate(['/home']);
        })
      } else {
        // show error
        this.showError();
      }
    });
  }

  setInitialFormFields() {
    this.editIncomeForm = new FormGroup({
      description: new FormControl('', Validators.required),
      amount: new FormControl(0, [Validators.required, Validators.min(0)])
    });
  }

}
