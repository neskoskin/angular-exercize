import { PostsService } from '../../services/posts.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RouterModule, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  myForm: FormGroup;
  // showError = false;
  // showSuccess = false;

  constructor(private service: PostsService, private router: Router, private toastr: ToastrService) { }

  showSuccess() {
    return this.toastr.success('Post successfully added!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('everything is broken', 'Major Error', {
      timeOut: 3000
    });
  }

  // addPost() {
  //   this.myForm.value.userId = 10;

  //   this.service.addPost(this.myForm.value)
  //     .subscribe(response => {
  //       if (response.status === 201) {
  //         this.showSuccess().onHidden.subscribe(res => {
  //           this.router.navigate(['/home']);
  //         })

  //       } else {
  //         // show error
  //         this.showError();
  //       }
  //     });
  // }

  ngOnInit() {
    this.myForm = new FormGroup({
      userId: new FormControl(),
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required)
    });
  }

}
