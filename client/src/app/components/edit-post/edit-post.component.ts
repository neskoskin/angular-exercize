import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  id: '';
  public myForm: FormGroup = null;
  showError = false;
  showSuccess = false;

  constructor(private postService: PostsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.setInitialFormFields();
    this.fetchPost(this.id);
  }

  fetchPost(id) {
    this.postService.fetchPost(id).subscribe(response => {
      const data = JSON.parse(response['_body']);

      this.myForm.controls.title.setValue(data.title);

      this.myForm.controls.body.setValue(data.body);
    });
  }

  // updatePost() {
  //   this.postService.updatePost(this.id, this.myForm.value).subscribe(response => {
  //     if (response.status === 200) {
  //       this.showError = false;
  //       this.showSuccess = true;
  //       // router nav to all post
  //       this.router.navigate(['/home']);

  //     } else {
  //       // show error
  //       this.showError = true;
  //       this.showSuccess = false;
  //     }
  //   });
  // }

  setInitialFormFields() {
    this.myForm = new FormGroup({
      userId: new FormControl(),
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required)
    });
  }



}
