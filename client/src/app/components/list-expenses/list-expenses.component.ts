import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ExpensesService } from '../../services/expenses.service';
import { Expense } from '../../models/expense.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-expenses',
  templateUrl: './list-expenses.component.html',
  styleUrls: ['./list-expenses.component.css']
})
export class ListExpensesComponent implements OnInit {
  expenses: Expense[];
  sumExpenses: number;
  @Output() expenseSumChanged: EventEmitter<number> = new EventEmitter();

  modalRef: BsModalRef;
  message: string;

  constructor(
    private expensesService: ExpensesService,
    private modalService: BsModalService,
    private toastr: ToastrService, ) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }
  showSuccess() {
    return this.toastr.success('Expense successfully deleted!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }
  private fetchExpenses() {
    this.expensesService.listExpenses()
      .subscribe(response => {
        this.sumExpenses = response.sum[0].amount;
        this.expenses = response.data;
        this.expenseSumChanged.emit(this.sumExpenses);
      });
  }

  deleteExpense(id) {
    this.expensesService.deleteExpense(id).subscribe(response => {
      if (response.status === 204) {
        this.showSuccess()
        this.fetchExpenses();
        this.modalRef.hide();
      } else {
        // show error
        this.modalRef.hide();
        this.showError();
      }
    });
  }

  ngOnInit() {
    this.expensesService.expenseValueChange.subscribe(value => {
      console.log('fetch expenses');
      this.fetchExpenses();
    })
  }
}