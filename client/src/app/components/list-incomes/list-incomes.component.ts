import { Component, OnInit, TemplateRef, Output, EventEmitter, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { IncomesService } from '../../services/incomes.service';
import { Income } from '../../models/income.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-incomes',
  templateUrl: './list-incomes.component.html',
  styleUrls: ['./list-incomes.component.css']
})
export class ListIncomesComponent implements OnInit {
  incomes: Income[];
  sumIncomes: number;
  @Output() incomeSumChanged: EventEmitter<number> = new EventEmitter();
  modalRef: BsModalRef;
  message: string;

  constructor(private incomesService: IncomesService,
    private modalService: BsModalService,
    private toastr: ToastrService, ) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  private fetchIncomes() {
    this.incomesService.listIncomes()
      .subscribe(response => {
          this.sumIncomes = response.sum[0].amount;
          console.log('expenses data', this.sumIncomes);
          this.incomes = response.data;
          this.incomeSumChanged.emit(this.sumIncomes);
      });
  }
  showSuccess() {
    return this.toastr.success('Income successfully deleted!', 'Success', {
      timeOut: 3000
    });
  }

  showError() {
    return this.toastr.error('Something went wrong!', 'Error', {
      timeOut: 3000
    });
  }
  deleteIncome(id) {
    this.incomesService.deleteIncome(id).subscribe(response => {
      if (response.status === 204) {
        this.showSuccess();
        this.fetchIncomes();
        this.modalRef.hide();
      } else {
        // show error
        this.modalRef.hide();
        this.showError();
      }
    });
  }

  ngOnInit() {
    this.incomesService.incomeValueChange.subscribe(value => {
      this.fetchIncomes();
    })
  }
}
