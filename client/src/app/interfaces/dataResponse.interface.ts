export interface DataResponse<T> {
    count: number;
    limit?: number;
    skip?: number;
    data: T;
    sum: T;
}