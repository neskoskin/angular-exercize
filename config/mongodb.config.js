'use strict';

// Import mongoose lib
const mongoose = require('mongoose'),
    config = require('./env.config');

module.exports = () => {
    // Connect and authenticate to db
    mongoose.connect(config.mongodb) // process.env.MONGO_URL || 'mongodb://crashoveride:admin123@ds237120.mlab.com:37120/accountdb') 
        .then(data => {
            console.log('We have connection to Mongodb');
        })
        .catch(error => {
            console.log('We have a problem connecting to Mongodb', error);
        });
}