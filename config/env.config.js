'use strict';

const glob = require('glob');
if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
}
const envConfig = glob.sync('./config/env/' + process.env.NODE_ENV + '.env.js');

module.exports = Object.assign(
    {},
    require('./env/all.env'),
    (Array.isArray(envConfig) && envConfig.length > 0) ? require('./env/' + process.env.NODE_ENV + '.env') : {}
);