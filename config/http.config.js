'use strict';

const express = require('express'),

    // glob is for searching files
    glob = require('glob'),
    // for json request and response
    bodyParser = require('body-parser'),
    // for module paths
    path = require('path'),
    // for cross origin requests
    cors = require('cors');

module.exports = () => {
    const isProduction = process.env.NODE_ENV === 'production';

    // Init ExpressJS
    const app = express();

    // Disable x-Powered-By header
    app.set('x-powered-by', false);
    // Set json api
    express.json();

    // Set bodyparser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    // Tell express to use json
    app.use(bodyParser.json());
    if (!isProduction) {
        // Cors enabler
        app.use(cors());
    }
    // Show stack errors
    app.set('showStackError', !isProduction);

    // Enable jsonp callbacks
    app.enable('jsonp callback');
    // Set up port and start listening
    const server = app.listen(process.env.PORT || 3000, () => {
        console.log('Server started');
    });
    app.set('server', server);

    // Setup models
    const models = glob('./app/models/**/*.js', {
        sync: true
    });
    // Setup routes
    const routes = glob('./app/routes/**/*.js', {
        sync: true
    });
    // Resolving modules
    models.map(match => require(path.resolve(match)));
    routes.map(match => require(path.resolve(match))(app));
}
