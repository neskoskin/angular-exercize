'use strict';

// Starting the HTTP server
require('./config/http.config')();

// Start the MongoDb
require('./config/mongodb.config')();
