'use strict';

const mongoose = require('mongoose'),
Schema = mongoose.Schema;

const ExpenseSchema = new Schema({
    description: {
        type: String,
        required: true,
        trim: true
    },
    // type: {
    //     type: String,
    //     required: true
    // },
    amount: {
        type: Number,
    },
    created: {
        type: Date,
        default: Date.now
    }
});

// We don't want to have __v property in plain objects and json
ExpenseSchema.set('toJSON', {getters: true, virtuals:true, versionKey: false});
ExpenseSchema.set('toObject', {getters: true, virtuals:true, versionKey: false});

// Register schema
mongoose.model('Expense', ExpenseSchema);