'use strict';
const expenses = require('../controllers/expenses.controller');

module.exports = (app) => {
    app.get('/expenses/', expenses.getAll);
    app.get('/expense/:expenseId', expenses.get);
    app.post('/expense', expenses.create);
    app.post('/expense/:expenseId', expenses.update);
    app.delete('/expense/:expenseId', expenses.delete);
    app.param('expenseId', expenses.expenseById);
}