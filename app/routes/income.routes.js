'use strict';
const incomes = require('../controllers/incomes.controller');

module.exports = (app) => {
    app.get('/incomes/', incomes.getAll);
    app.get('/income/:incomeId', incomes.get);
    app.post('/income', incomes.create);
    app.post('/income/:incomeId', incomes.update);
    app.delete('/income/:incomeId', incomes.delete);

    app.param('incomeId', incomes.incomeById);
}