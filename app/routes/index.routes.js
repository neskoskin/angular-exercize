'use strict';

const index = require('../controllers/index.controller');

module.exports = (app) => {
    app.get('/', index.start);
    app.get('/Hello', index.hello);
}