'use strict';
const ExpenseModel = require('mongoose').model('Expense'),
    escapeRegExp = require('lodash/escapeRegExp');

exports.getAll = (req, res) => {
    const findQuery = {};
    const skip = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || Number.MAX_SAFE_INTEGER;

    if (req.query.search) {
        const re = new RegExp(escapeRegExp(req.query.search), 'i');
        findQuery.$or = [
            { name: { $regex: re } },
            { type: { $regex: re } },
            { amount: { $regex: re } },
        ]
    }
    const find = ExpenseModel.find(findQuery)
        .sort({ name: 1 })
        .skip(skip)
        .limit(limit)
        .lean()

    const count = ExpenseModel.count(findQuery)
    const sum = ExpenseModel.aggregate([{ $group: { _id: null, amount: { $sum: "$amount" } } }]);
 
    Promise.all([find, count, sum])
        .then(data => {
            res.json({
                skip: skip,
                limit: limit,
                count: data[1],
                data: data[0],
                sum: data[2]
            })
        }).catch(error => {
            console.log(error);
            res.status(400).json({ error: error });
        })
}

exports.get = (req, res) => {
    res.json(req.data.expense);
}
exports.create = (req, res) => {
    const Expense = new ExpenseModel(req.body);
    Expense.save()
        .then(expense => res.json(expense))
        .catch(error => {
            console.error(error);
            res.status(400).json({ error: error });
        })
}
exports.update = (req, res) => {
    console.log('in update');
    const Expense = req.data.expense;
    Object.assign(Expense, req.body);
    Expense.save()
        .then(expense => res.json(expense))
        .catch(error => {
            console.log(error);
            res.status(400).json({ error: error });
        })
}
exports.expenseById = (req, res, next, id) => {
    ExpenseModel.findById(id)
        .then(expense => {
            req.data = { expense: expense };
            return next();
        })
        .catch(error => {
            console.error(error);
            next(error);
        })
}
exports.delete = (req, res, next) => {
    const id = '';
    if (req.data.expense !== null) {
        ExpenseModel.remove({ _id: req.data.expense._id })
            .then(response => {
                res.json({ success: true, status: 204 })
            })
            .catch(error => {
                console.error(error);
                next(error);
            })
    }
}
