'use strict';
const IncomeModel = require('mongoose').model('Income'),
    escapeRegExp = require('lodash/escapeRegExp');

exports.getAll = (req, res) => {
    const findQuery = {};
    const skip = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || Number.MAX_SAFE_INTEGER;

    if (req.query.search) {
        const re = new RegExp(escapeRegExp(req.query.search), 'i');
        findQuery.$or = [
            { name: { $regex: re } },
            { type: { $regex: re } },
            { amount: { $regex: re } },
        ]
    }
    const find = IncomeModel.find(findQuery)
        .sort({ name: 1 })
        .skip(skip)
        .limit(limit)
        .lean()

    const count = IncomeModel.count(findQuery)
    const sum = IncomeModel.aggregate([{ $group: { _id: null, amount: { $sum: "$amount" } } }]);

    Promise.all([find, count, sum])
        .then(data => {
            res.json({
                skip: skip,
                limit: limit,
                count: data[1],
                data: data[0],
                sum: data[2]
            })
        }).catch(error => {
            console.log(error);
            res.status(400).json({ error: error });
        })
}

exports.get = (req, res) => {
    res.json(req.data.income);
}
exports.create = (req, res) => {
    const Income = new IncomeModel(req.body);
    Income.save()
        .then(income => res.json(income))
        .catch(error => {
            console.error(error);
            res.status(400).json({ error: error });
        })
}
exports.update = (req, res) => {
    const Income = req.data.income;
    Object.assign(Income, req.body);
    Income.save()
        .then(income => res.json(income))
        .catch(error => {
            console.log(error);
            res.status(400).json({ error: error });
        })
}
exports.incomeById = (req, res, next, id) => {
    IncomeModel.findById(id)
        .then(income => {
            req.data = { income: income };
            return next();
        })
        .catch(error => {
            console.error(error);
            next(error);
        })
}
exports.delete = (req, res, next) => {
    const id = '';
    if (req.data.income !== null) {
        IncomeModel.remove({ _id: req.data.income._id })
            .then(response => {
                res.json({ success: true, status: 204 })
            })
            .catch(error => {
                console.error(error);
                next(error);
            })
    }
}